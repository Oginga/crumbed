from __future__ import unicode_literals

from django.apps import AppConfig


class PaymentCliConfig(AppConfig):
    name = 'payment_cli'
